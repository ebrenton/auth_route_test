

$.urlParam = function (name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href) || ['',''];
	return results[1];
}

function formatDate(datetime, dateOnly) {
    // see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat
    var dt = new Date(Date.parse(datetime));
    if (!isNaN(dt))
        if(dateOnly)  //use UTC time zone since we only care about the date here. Do not adjust per timeZone
            return Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit', timeZone: 'UTC' }).format(dt);
        else
            return Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit', hour: 'numeric', minute: 'numeric' }).format(dt);
    else
        return '-invalid date-';
}

function formatNumber(nStr, decPlaces, addCommas) {
    if (isNaN(nStr)) return '';
    var newStr = Number.parseFloat(nStr).toFixed(decPlaces).toString();
    //nStr += '';
    if (addCommas) {
        x = newStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        newStr = x1 + x2;
    }
    return newStr;
}

function getFileType(fname) {

    if (!fname) return 'unknown';

    const knowTypes = ['7z', 'aac', 'ai', 'archive', 'ajr', 'audio', 'avi', 'css', 'csv', 'dbf', 'doc', 'docx', 'dwg', 'exe', 'fla', 'flac', 'gif', 'html', 'iso', 'jpg', 'js', 'json',
        'mdf', 'mp2', 'mp3', 'mp4', 'msg', 'mxf', 'nrg', 'pdf', 'png', 'ppt', 'psd', 'rar', 'rtf', 'svg', 'text', 'tiff', 'txt', 'video', 'wav', 'wma', 'xls', 'xlsx', 'xml', 'zip'];

    var ext = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2).toLowerCase();
    if (knowTypes.indexOf(ext) == -1) ext = 'unknown';

    return ext;
}

function incrementValue(obj, inc) {
    if (!obj) return;
    objVal = parseInt(obj.val(), 10);
    minVal = obj.attr('min');
    maxVal = obj.attr('max');
    if (isNaN(objVal)) {
        if (minVal) {
            obj.val(minVal);
        } else {
            obj.val(0);
        }
        return;
    }
    if (maxVal && objVal + inc > maxVal) {
        obj.val(maxVal);
        return;
    }
    if (minVal && objVal + inc < minVal) {
        obj.val(minVal);
        return;
    }
    obj.val(objVal + inc);
}

function lookupCompany(compName, locTypeStr, cb) {

    var url = '/company/search/' + encodeURIComponent(compName);
    if (locTypeStr) url += '/' + encodeURIComponent(locTypeStr);

    $.ajax({
        url: url,
        success: function (result) {
            if (result.ok) {
                //return cb(result.loggedin, result.username, result.authurl);
                //var comp = {};
                if (result.Company) {
                    return cb(true, result, null);
                } else {
                    er = new Error('Company not found');
                    return cb(false, result, er);
                }
            } else {
                //alert('error in lookupCompany: ' + JSON.stringify(result));
                er = new Error('Error ruturned from company search:'+JSON.stringify(result))
                return cb(false, null, er);
            }
        },
        error: function (jqXHR, status, error) {
            //alert(jqXHR.responseText);
            er = new Error('Error in lookupCompany:' + error.message);
            return cb(false, null, er);
        }
    });
}