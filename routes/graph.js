const express = require('express');
const router = express.Router();
require("isomorphic-fetch"); //required for microsoft-graph-client
const graph = require('@microsoft/microsoft-graph-client');
const auth = require('./auth');

const oauth2credentials = {
    client: {
        id: process.env.AZURE_CLIENT_ID,
        secret: process.env.AZURE_CLIENT_SECRET
    },
    auth: {
        tokenHost: 'https://login.microsoftonline.com',
        authorizePath: 'common/oauth2/v2.0/authorize',
        tokenPath: 'common/oauth2/v2.0/token'
    }
};

//const oauth2 = require('simple-oauth2').create(oauth2credentials);

router.get('/', async function (req, res, next) {

    if (!req.user || !req.user.azureAccessToken) {
        res.send('Azure user not logged in<br /><br /><a href="/">Home</a>');
    } else {

        auth.getAzureAccessToken(req, null, null, null, (err, accessToken) => {

            if (err) return next(err);

            if (!accessToken) return res.send('failed to get access token in checkUserAzureToken()');

            getUser(accessToken, (userObj) => {
                console.log('got azure me: ' + JSON.stringify(userObj));
                res.send('ok<br /><pre>' + JSON.stringify(userObj) + '</pre><br /><br /><a href="/">Home</a>');

            });

        });
        
    }
});

module.exports = router;

//async function getUserAzureToken(req, cb) {


//    //if we dont have the required info return a null
//    if (!req.user.azureAccessToken || !req.user.azureAccessTokenExpires || !req.user.azureRefreshToken) return cb(null);
//    //if the token has not expired then return the current token
//    if (new Date().getTime() < req.user.azureAccessTokenExpires) return cb(req.user.azureAccessToken);

//    //if the token has expired then try tpo get a new one with the refresh token.
//    console.log('generating a new azure access token using the refresh token');
//    var newToken = await oauth2.accessToken.create({ refresh_token: req.user.azureRefreshToken }).refresh();

//    if (newToken) {
//        console.log('new access token generated from refresh token');
//        //console.log('new token:\n' + JSON.stringify(newToken));

//        //update the user record with the new token information
//        req.user.azureAccessToken = newToken.token.access_token;
//        req.user.azureAccessTokenExpires = parseFloat(new Date().getTime()) + 30000//3000000; //intentionally 3000 instead of 3600 to allow room for clock differences
//        req.user.azureRefreshToken = newToken.token.refresh_token;
        
        
//        //req.login is exposed by passport - it updates the session and passport objects with the specified user. It calls passport.serializeUser()
//        //================================
//        req.login(req.user, (err) => {
//            if (err) return next(err);
//            cb(newToken.token.access_token);
//        });
//    } else {
//        return cb(null);
//    }

//}

async function getUser(accessToken, cb) {

    var client = null;

    try {
        client = graph.Client.init({
            authProvider: (done) => {
                done(null, accessToken);
            }
        });    
    } catch (err) {
        console.log(err);
        if (cb) {
            return cb({ error: err });
        } else {
            return { error: err };
        }
    }

    try {
        const result = await client.api('/me').get();
        if (cb) {
            return cb(result);
        } else {
            return result;
        }
    } catch (err) {
        console.log(err);
        if (cb) {
            return cb({ error: err });
        } else {
            return { error: err };
        }
    }

}