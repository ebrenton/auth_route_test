var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {

    var curStr //= req.isAuthenticated() ? 'Currently logged in as ' + req.user.username : 'Not logged in';
    var isLoggedIn = req.isAuthenticated();

    var title = 'Express + Passport Test';

    if (isLoggedIn) {
        var liMethod = 'Local Login';
        if (req.user.azureProfile) liMethod = 'Azure';
        if (req.user.fbProfile) liMethod = 'Facebook';
        if (req.user.googleProfile) liMethod = 'Google';

        res.render('index', { title: title, userName: req.user.username, email: req.user.email, loginMethod: liMethod, restrictAll: process.env.RESTRICT_ALL_IF_NOT_LOGGED_IN });
        
    } else {
        //curStr = 'Not logged in';
        //isLoggedIn = false;
        res.render('index_not_logged_in', { title: title });
    }
    
    //res.render('index', { title: 'Express + Passport Test', uNote: curStr, loggedIn: isLoggedIn, restrictAll: process.env.RESTRICT_ALL_IF_NOT_LOGGED_IN});
});

module.exports = router;
