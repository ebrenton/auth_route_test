const express = require('express');
const router = express.Router();
const request = require('request');


//Passport Strategies
//=====================
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
//const AzureOAuth2Strategy = require("passport-azure-oauth2"); 
//Installing 'passport-azure-ad' via npm threw several ERRORS about Python, but everything seems to be working anyway.
//const BearerStrategy = require('passport-azure-ad').BearerStrategy
const OIDCStrategy = require('passport-azure-ad').OIDCStrategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;

//var azureEndpoint = 'https://login.microsoftonline.com/seielect.onmicrosoft.com/v2.0/.well-known/openid-configuration';
var azureEndpoint = 'https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration';

passport.use('azuread-openidconnect', new OIDCStrategy({
    identityMetadata: azureEndpoint,
    clientID: process.env.AZURE_CLIENT_ID,
    responseType: 'code id_token',
    responseMode: 'form_post',
    redirectUrl: process.env.AZURE_CLIENT_CALLBACK,
    allowHttpForRedirectUrl: false,
    clientSecret: process.env.AZURE_CLIENT_SECRET,
    validateIssuer: false,
    isB2C: false,
    issuer: '',
    passReqToCallback: true,
    scope: ['openid', 'profile', 'offline_access', 'User.Read', 'Mail.Read', 'Calendars.Read', 'Contacts.Read', 'People.Read'],
    loggingLevel: 'warn',
    loggingNoPII: false,
    nonceLifetime: 3600,
    nonceMaxAmount: 10,
    useCookieInsteadOfSession: false,
    cookieEncryptionKeys: null,
    clockSkew: 300
},
    function (req, iss, sub, profile, accessToken, refreshToken, done) {
        if (!profile.oid) {
            return done(new Error("No oid found"), null);
        }
        //// asynchronous verification, for effect...
        //process.nextTick(function () {
        //    findByOid(profile.oid, function (err, user) {
        //        if (err) {
        //            return done(err);
        //        }
        //        if (!user) {
        //            // "Auto-registration"
        //            users.push(profile);
        //            return done(null, profile);
        //        }
        //        return done(null, user);
        //    });
        //});
        //console.log('azure openid profile:' + JSON.stringify(profile));
        //var user = jwt.decode(params.id_token, "", true);

        profile = profile._json;

        var userAccessToken = accessToken; // jwt.decode(params.access_token, "", true);
        var userRefreshToken = refreshToken; // jwt.decode(refreshtoken, "", true);
        var userAccessTokenExpires = parseFloat(new Date().getTime()) + 3000000; //intentionally 3000 instead of 3600 to allow room for error or clock diferences
        user = { username: profile.name, email: profile.preferred_username, azureID: profile.oid, id: 444, azureProfile: profile, azureAccessToken: userAccessToken, azureAccessTokenExpires: userAccessTokenExpires, azureRefreshToken: userRefreshToken };
        //console.log('Got Azure OpenID User:\n', JSON.stringify(user));
        //console.log('Got Azure Profile:', JSON.stringify(profile));
        getStackpoleUser('azure',userAccessToken,user,done);
        //done(null, user);
    }
));

passport.use(new FacebookStrategy({
    clientID: process.env.FB_CLIENT_ID,
    clientSecret: process.env.FB_CLIENT_SECRET,
    callbackURL: process.env.FB_CLIENT_CALLBACK,
    profileFields: ['id', 'emails', 'name']
},
    function (accessToken, refreshToken, profile, done) {
        //User.findOrCreate({ facebookId: profile.id }, function (err, user) {
        //    return done(err, user);
        //});
        if (profile.id) {
            //var fbAccessTokenExpires = parseFloat(new Date().getTime()) - 30000 //exire this short lived token immediately!!!!+ 30000//3000000; //intentionally 3000 instead of 3600 to allow room for error or clock diferences
            //the initial token from fb is short-lived, so immediately call this to get a long-lived token.
            //req.user.fbAccessToken = accessToken;
            //req.user.fbAccessTokenExpires = parseFloat(new Date().getTime()) - 30000 //in the past...pre-expired
            getFacebookToken(null, accessToken, 1, (err, fbToken) => {
                if (err) return done(err, false);
                user = { username: profile.displayName, email: profile.emails[0].value, facebookID: profile.id, id: 333, fbProfile: profile, fbAccessToken: fbToken, fbAccessTokenExpires: parseFloat(new Date().getTime()) + 3600000 * 24 };
                //console.log('got Facebook user:\n' + JSON.stringify(user));
                getStackpoleUser('facebook',fbToken,user,done);
                //return done(null, user);
            });

            //user = { username: profile.displayName, facebookID: profile.id, id: 333, fbProfile: profile, fbAccessToken: accessToken, fbAccessTokenExpires: fbAccessTokenExpires };
            //console.log('got Facebook user:\n' + JSON.stringify(user));
            //return done(null, user);
        } else {
            //let e = new Error('invalid fb user, no profile id or fb login failed');
            //return done(e, false);

            //Do NOT send an error if you are using "failureRedirect" in the call to passport.authenticate
            //Example: passport.authenticate('facebook', { failureRedirect: '/ptest/fail' }),...
            //if you return an error in the first param the page will show the error and the redirect will not get called.
            return done(null, null);
        }
    }
));

passport.use('google',new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.GOOGLE_CLIENT_CALLBACK,
    passReqToCallback: true
},
    function (req, accessToken, refreshToken, params, profile, done) {
        //User.findOrCreate({ googleId: profile.id }, function (err, user) {
        //    return cb(err, user);
        //});
        //console.log('Google params:\n' + JSON.stringify(params));

        if (profile.id) {
            var userAccessTokenExpires = parseFloat(new Date().getTime()) + ((params.expires_in * 1000) - 300000); //subtract 5 min to allow for clock difs
            //var userAccessTokenExpires = parseFloat(new Date().getTime()) + 10000;
            user = { username: profile.displayName, email: profile.emails[0].value, googleID: profile.id, id: 555, googleProfile: profile._json, googleAccessToken: accessToken, googleAccessTokenExpires: userAccessTokenExpires, googleRefreshToken: refreshToken };
            console.log('got Google user:\n' + JSON.stringify(user));
            getStackpoleUser('google', accessToken,user,done);
            //return done(null, user);
        } else {
            //let e = new Error('invalid fb user, no profile id or fb login failed');
            //return done(e, false);

            //Do NOT send an error if you are using "failureRedirect" in the call to passport.authenticate
            //Example: passport.authenticate('facebook', { failureRedirect: '/ptest/fail' }),...
            //if you return an error in the first param the page will show the error and the redirect will not get called.
            return done(null, null);
        }
    }
));

passport.use(new LocalStrategy(
    function (username, password, done) {
        console.log('inside LocalStrategy.');

        stackpoleUserCheck(username, password, (err, user) => {
            if (err) return done(err, false);
            return done(null, user);
        });
        //var user = null;
        //if (username === 'test' && password === 'test') {
        //    user = { username: username, email: 'test@here.com', password: password, id: 111 };
        //    //console.log('inside LocalStrategy. user=' + JSON.stringify(user));
        //    //return done(null, user);
        //} else if (username === 'xxx' && password === 'xxx') {
        //    user = { username: username, email: 'xxx@here.com', password: password, id: 222 };
        //    //console.log('inside LocalStrategy. user=' + JSON.stringify(user));
        //    //return done(null, user);
        //} //else {
        ////return done(null, false);
        ////}

        //if (user) {
        //    //return done(null, user);
        //    //process.nextTick(function () {
        //    console.log('LocalStrategy succeeded. user=' + JSON.stringify(user));
        //    return done(null, user);
        //    //});
        //} else {
        //    console.log('LocalStrategy Failed.');

        //    //let e = new Error('invalid user id or password');
        //    //return done(e, false);

        //    //Do NOT send an error if you are using "failureRedirect" in the call to passport.authenticate
        //    //Example: passport.authenticate('local', { failureRedirect: '/ptest/fail' }),...
        //    //if you return an error in the first param the page will show the error and the redirect will not get called.
        //    return done(null, null);
        //}
    }
));

//========================
//End Passport Strateigies
//========================

//Passport Serializea and Deserialize
//===================================
passport.serializeUser(function (user, done) {
    //this would normally be the place where we would know the id from the db lookup and use that to "serialize" the user
    //for this test we will just set the id...
    console.log('inside serializeUser.');
    //console.log(JSON.stringify(user));
    //done(null, user.id);
    done(null, user);
});

//deserializeUser gets called on every page request IF a passport object exists in the session
//instead of a full reauthentication, it uses the saved data and we can use that to make sure the user is still valid
//the first param will be whatever was set in the callback (done()) function of serializeUser
passport.deserializeUser(function (user, done) { 

    //normally we would RE-look up the user in the database based on the supplied "id"
    //for this test we will just check for id 111 or 222, etc

    //console.log('inside deserializeUser. user=' + JSON.stringify(user));
    
    //var id = user.id;

    //if (id === 111) {
    //    user = { username: 'test', email: user.email, password: 'this don\'t matter', id: id };
    //} else if (id === 222) {
    //    user = { username: 'xxx', email: user.email, password: 'doesn\'t matter', id: id };
    //} else if (id === 333 || id === 444 || id === 555) {  //Facebook, Azure, Google
    //    //user = { username: user.username, password: 'doesn\'t matter', id: id }; //just let user = user
    //} else {
    //    console.log('deserializeUser failed');
    //    let e = new Error('deserialize error - invalid user id');
    //    return done(e, false);

    //    ////THEORY
    //    ////If deserializeUser fails it means a passport session is set/established, but it is bad...
    //    ////so, we could just kill the session and send them to somewhere like a login page.
    //    //// *** Now handling this in main error trap in app.js ***

    //}

    ////console.log('deserializeUser succeeded. user: ' + JSON.stringify(user));
    ////

    //***Just return the user without any validation for the moment.***
    if (!user) {
        let e = new Error('no user object');
        return done(e, false);
    }
    console.log('deserializeUser succeeded.');
    done(null, user);
});


//Routes
//=======


router.get('/', function (req, res) {

    //This is just a test route


    //res.redirect('/success?username=' + req.user.username);
    //console.log('successful get. user:' + req.user);
    //res.send('logged in');
    //res.send('logged in as ' + req.user.username);

    var rStr = 'This tests a backend page/route.<br />Using req.isAuthenticated() to determine if the user is logged in or not.'
    rStr += '<br />====================================================================<br /> <br />';

    if (req.isAuthenticated()) {

        console.log('called auth with req.user: ' + JSON.stringify(req.user));

        rStr += 'Logged in as ' + req.user.username + ' xmlKey: ' + req.user.xmlReqToken;
        if (req.user.fbProfile) {
            rStr += '<br />Logged in with Facebook.<br />Facebook id:' + req.user.fbProfile.id;
            rStr += '<br /><br /><a href="/auth/fblogout">Log Out (and logout of Facebook)</a> (not really necessary but good for testing)<br />';
            rStr += '<br /><a href="/auth/fbtest">Facebook API Test</a><br />';
            rStr += '<a href="/auth/fbtokenrefresh">Facebook Token Refresh</a><br />';
        }
        if (req.user.googleProfile) {
            rStr += '<br />Logged in with Google.<br />Google id:' + req.user.googleProfile.id;
            rStr += '<br /><br /><a href="/auth/googletest">Google API Test</a><br />';
            //rStr += '<br /><br /><a href="/auth/fblogout">Log Out (and logout of Facebook)</a> (not really necessary but good for testing)<br />';
        }
        if (req.user.azureProfile) {
            rStr += '<br />Logged in with Azure.<br />Azure id:' + req.user.azureID;
            //rStr += '<br /><br />Azure Access Token:' + req.user.azureAccessToken;
            //rStr += '<br /><br />Azure Refresh Token:' + req.user.AzureRefreshToken;
            rStr += '<br /><br /><a href="/auth/azureopenidtest">Azure Test</a> (without graph-client)';
            rStr += '<br /><br /><a href="/graph">Azure Graph Client Test</a><br />'; 
        }

        rStr += '<br /><br /><a href="/auth/logout">Log Out</a> (destroy node session and delete connect.sid cookie from client)<br /><br /><a href="/">Home</a>';

        res.send(rStr);
    } else {
        rStr += 'Not logged in!<br /><br /><a href="/auth/loginform">Login</a><br /><br /><a href="/">Home</a>';
        res.send(rStr);
    }
});

//Local Login Form Handling
//=========================
//this is called (posted) from the loginform.html login page
router.post('/locallogin', passport.authenticate('local', { failureRedirect: '/auth/fail' }), function (req, res) {
    //res.redirect('/success?username=' + req.user.username);
    console.log('successful post login. user:' + JSON.stringify(req.user));

    //IMPORTANT
    //=========
    //Save the session first... otherwise the browser may load the redirect page before the session is saved and it wont have the user info yet!!!
    req.session.save(() => {
        process.nextTick(() => { //there's still a timing issue that causes the headers to be rewritten here intermittently
            return res.redirect('/'); //the return is probably irrelevant since the surrounding functions are asynchronous
        });
    });
});

//=============
//Azure OPENID
//=============
router.get('/azureopenidlogin', passport.authenticate('azuread-openidconnect', { successRedirect: '/' }));

//Note: Using POST here
//Full callback path for local testing = https://localhost:3444/auth/azureopenidcallback (must be setup in app settings on Azure)
router.post('/azureopenidcallback', passport.authenticate('azuread-openidconnect', { failureRedirect: '/auth/azureopenidfail' }), (req, res) => {

    //IMPORTANT - Save the session first... otherwise the browser may load the redirect page before the session is saved and it wont have the user info yet!!!
    //=========
    req.session.save(() => {
        res.redirect('/');
    });
});

router.get('/azureopenidfail', function (req, res) {
    res.send('Azure login failed<br /><a href="/">Home</a>');
});

router.get('/azureopenidtest', (req, res) => {

    console.log('in azureopenidtest');
    console.log('azureID:' + req.user.azureID);
    
    
    getAzureAccessToken(req, null, null, null, (err, accessToken) => {

        if (err) return next(err);

        console.log('azureAccessToken:' + accessToken);

        const options = {
            method: 'GET',
            uri: 'https://graph.microsoft.com/v1.0/me',
            headers: {
                Authorization: ' Bearer ' + accessToken
            }
        };
        request(options, (err, resp, body) => {
            if (err) return next(err);

            if (resp.statusCode !== 200) {
                res.status = resp.statusCode;
                res.send('Azure test failed:<br/>' + JSON.stringify(resp));
                return;
            }

            //res.send(rStr);
            res.json(body);

        });

    });

});

//=================
//End Azure routes
//=================


//================
//Facebook routes
//================
router.get('/fblogin', passport.authenticate('facebook', { scope: ['public_profile', 'email'] }));

// handle the callback after facebook has authenticated the user
// Full callback path for local testing = https://localhost:3444/auth/fbcallback (must be setup in app settings on Facebook)
router.get('/fbcallback', passport.authenticate('facebook', { failureRedirect: '/auth/fbfail' }), (req, res) => {

    //IMPORTANT - Save the session first... otherwise the browser may load the redirect page before the session is saved and it wont have the user info yet!!!
    //=========
    req.session.save(() => {
        res.redirect('/');
    });
});

//This will log the user out of Facebook after logging them out of the local session
router.get('/fblogout', (req, res) => {

    let fbAccessToken = req.user ? req.user.fbAccessToken : null;

    if (req.session) {
        req.session.destroy();
        res.clearCookie('connect.sid');
        console.log('session destroyed and cookie deleted in fblogout');
    }

    if (fbAccessToken) {
        //only use this link if a facebook session existed
        console.log('logging out of facebook');
        var redir = 'https://www.facebook.com/logout.php?next=' + process.env.FB_LOGOUT_CALLBACK + '&access_token=' + fbAccessToken
        console.log('redirecting to ' + redir);
        res.redirect(redir);
    } else {
        res.redirect('/');
        //req.session.save(() => {
        //    res.redirect('/');
        //});
    }
});

router.get('/fbfail', function (req, res) {
    res.send('Facebook login process failed<br /><a href="/">Home</a>');
});

router.get('/fbtokenrefresh', (req, res) => {

    console.log('in fbtokenrefresh');
    console.log('facebookID:' + req.user.facebookID);
    console.log('fbAccessToken:' + req.user.fbAccessToken);

    const options = {
        method: 'GET',
        uri: 'https://graph.facebook.com/v4.0/oauth/access_token?grant_type=fb_exchange_token',
        qs: {
            client_id: process.env.FB_CLIENT_ID,
            client_secret: process.env.FB_CLIENT_SECRET,
            fb_exchange_token: req.user.fbAccessToken
        }
    };
    request(options, (err, resp, body) => {
        if (err) return next(err);

        if (resp.statusCode !== 200) {
            res.status = resp.statusCode;
            res.send('fbtokenrefresh failed:<br/>' + JSON.stringify(resp));
            return;
        }

        //var rData = JSON.parse(body);

        //var rStr = 'Facebook test successfull<br />';
        //rStr += 'id: ' + rData.id + '<br />';
        //rStr += 'name: ' + rData.name + '<br />';
        //rStr += 'email: ' + rData.email + '<br />';
        //rStr += '<br /><a href="/">Home</a>';

        //res.send(rStr);
        res.json(JSON.parse(body));
    });

});

router.get('/fbtest', (req, res) => {

    console.log('in fbtest');
    console.log('facebookID:' + req.user.facebookID);
    console.log('fbAccessToken:' + req.user.fbAccessToken);

    getFacebookToken(req, null, null, (err, fbToken) => {
        if (err) return next(err);

        const options = {
            method: 'GET',
            uri: 'https://graph.facebook.com/v4.0/me?fields=id,name,email',
            qs: {
                access_token: fbToken
            }
        };
        request(options, (err, resp, body) => {
            if (err) return next(err);

            if (resp.statusCode !== 200) {
                res.status = resp.statusCode;
                res.send('Facebook test failed:<br/>' + JSON.stringify(resp));
                return;
            }

            var rData = JSON.parse(body)

            var rStr = 'Facebook test successfull<br />';
            rStr += 'id: ' + rData.id + '<br />';
            rStr += 'name: ' + rData.name + '<br />';
            rStr += 'email: ' + rData.email + '<br />';
            rStr += '<br /><a href="/">Home</a>';

            res.send(rStr);
            //res.json(JSON.parse(body));
        });

    });

});

//===================
//End Facebook routes
//===================

//=======
//Google
//=======
var googleOptions = {
    scope: ['profile', 'email'],
    successRedirect: '/',
    accessType: 'offline'
};
//***Note*** A refresh token is only supplied the first time a user accepts the login access with Google
//If the user logs out of your app and the then back in with Google, the access was already granted and a new refresh token will not be supplied.
//To get around this we need to promt the user for consent every time they log in and then it WILL return a refresh token
googleOptions.prompt = 'consent'; //Without this, you don't get a refresh token...BUT it prompts the user every time???

router.get('/googlelogin', passport.authenticate('google', googleOptions));

//Note: Using POST here
//Full callback path for testing = https://localhost:3444/auth/googlecallback (must be setup in app settings on Azure)
router.get('/googlecallback', passport.authenticate('google', { failureRedirect: '/auth/googlefail' }), (req, res) => {

    //IMPORTANT - Save the session first... otherwise the browser may load the redirect page before the session is saved and it wont have the user info yet!!!
    //=========
    req.session.save(() => {
        res.redirect('/');
    });
});

router.get('/googlefail', function (req, res) {
    res.send('Google login failed<br /><a href="/">Home</a>');
});


router.get('/googletest', (req, res) => {

    console.log('in googletest');
    console.log('googleID:' + req.user.googleID);
    console.log('googleAccessToken:' + req.user.googleAccessToken);

    getGoogleAccessToken(req, null, null, null, (err, gToken) => {
        //if (err) return next(err);

        const options = {
            method: 'GET',
            uri: 'https://people.googleapis.com/v1/people/me',
            qs: {
                access_token: gToken,
                personFields: 'names,emailAddresses'
            }
        };
        request(options, (err, resp, body) => {
            if (err) return next(err);

            if (resp.statusCode !== 200) {
                res.status = resp.statusCode;
                res.send('Google test failed:<br/>' + JSON.stringify(resp));
                return;
            }

            //var rData = JSON.parse(body)

            //var rStr = 'Facebook test successfull<br />';
            //rStr += 'id: ' + rData.id + '<br />';
            //rStr += 'name: ' + rData.name + '<br />';
            //rStr += 'email: ' + rData.email + '<br />';
            //rStr += '<br /><a href="/">Home</a>';

            //res.send(rStr);
            res.json(body);
        });

    });

});
//=================
//End Google routes
//=================



router.get('/fail', function (req, res) {
    res.send('Local login failed<br /><a href="/">Home</a>');
});

router.get('/loginform', function (req, res) {
    res.render('loginform', { title: 'Express + Passport Login Form' });
});

router.get('/logout', (req, res) => {
    if (req.session) {
        req.session.destroy();
        res.clearCookie('connect.sid');
        console.log('session destroyed and cookie deleted');
        res.redirect('/');
    } else {
        var err = new Error('You are not logged in, you don\'t even have a session! How do you expect to log out? Doh!');
        err.status = 403;
        next(err);
    }
});

module.exports = router;



//Functions
//===================
module.exports.verifyLoggedIn = function (req, res, next) {

    if (req.isAuthenticated()) {
        next();
    } else {
        var err = new Error('User not authenticated');
        err.status = 403;
        next(err);
    }

};


//module.exports.getAzureAccessToken = function (req, tmpToken, tmpExp, tmpRefToken, cb) {
function getAzureAccessToken(req, tmpToken, tmpExp, tmpRefToken, cb) {

    if (!tmpToken && req) tmpToken = req.user.azureAccessToken;
    if (!tmpExp && req) tmpExp = req.user.azureAccessTokenExpires;
    if (!tmpRefToken && req) tmpRefToken = req.user.azureRefreshToken;

    //if we dont have the required info return a null
    if (!tmpToken || !tmpExp || !tmpRefToken) return cb(new Error('Some or all token info is missing'), null);
    //if the token has not expired then return the current token
    if (new Date().getTime() < tmpExp) return cb(null, tmpToken);

    //if the token has expired then try tpo get a new one with the refresh token.
    console.log('generating a new azure access token using the azure refresh token');

    //const options = {
    //    method: 'POST',
    //    uri: 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
    //    json: {
    //        grant_type: 'refresh_token',
    //        client_id: process.env.AZURE_CLIENT_ID,
    //        client_secret: process.env.AZURE_CLIENT_SECRET,
    //        redirect_uri: process.env.AZURE_CLIENT_CALLBACK,
    //        scope: 'openid%20profile%20offline_access%20User.Read%20Mail.Read%20Calendars.Read%20Contacts.Read%20People.Read',
    //        refresh_token: tmpRefToken,
    //    }
    //};

    var formStr = 'grant_type=refresh_token';
    formStr += '&client_id=' + encodeURIComponent(process.env.AZURE_CLIENT_ID);
    formStr += '&client_secret=' + encodeURIComponent(process.env.AZURE_CLIENT_SECRET);
    formStr += '&redirect_uri=' + encodeURIComponent(process.env.AZURE_CLIENT_CALLBACK);
    formStr += '&refresh_token=' + encodeURIComponent(tmpRefToken);
    //formStr += '&scope=' + 'openid%20profile%20offline_access%20User.Read%20Mail.Read%20Calendars.Read%20Contacts.Read%20People.Read';
    formStr += '&scope=' + encodeURIComponent('openid profile offline_access User.Read Mail.Read Calendars.Read Contacts.Read People.Read');

    const options = {
        method: 'POST',
        uri: 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
        headers: { 'content-type': 'application/x-www-form-urlencoded'},
        body: formStr
    };
    request(options, (err, resp, body) => {
        if (err) return cb(err, null);

        if (resp.statusCode !== 200) {
            let err = new Error(JSON.stringify(resp));
            return cb(err, null);
        }

        var rData = JSON.parse(body);

        console.log('new azure access token generated from refresh token');
        //console.log('new token:\n' + rData.access_token);
        console.log(JSON.stringify(rData));

        if (req) {
            //update the user record with the new token information
            req.user.azureAccessToken = rData.access_token;
            req.user.azureRefreshToken = rData.refresh_token;
            //req.user.googleAccessTokenExpires = parseFloat(new Date().getTime()) + 30000;
            req.user.azureAccessTokenExpires = parseFloat(new Date().getTime()) + ((rData.expires_in * 1000) - 300000); //subtract 5 min to allow for clock difs

            //req.login is exposed by passport - it updates the session and passport objects with the specified user. It calls passport.serializeUser()
            //================================
            req.login(req.user, (err) => {
                if (err) return cb(err, null);
                cb(null, rData.access_token);
            });
        } else {
            cb(null, rData.access_token);
        }

    });

}

module.exports.getAzureAccessToken = getAzureAccessToken;


function getFacebookToken(req, tmpToken, tmpExp, cb) {

    if (!tmpToken && req) tmpToken = req.user.fbAccessToken;
    if (!tmpExp && req) tmpExp = req.user.fbAccessTokenExpires;

    //if we dont have the required info return a null
    if (!tmpToken || !tmpExp) return cb(new Error('No token info'), null);
    //if the token has not expired then return the current token
    if (new Date().getTime() < tmpExp) return cb(null, tmpToken);

    //if the token has expired then try tpo get a new one with the refresh token.
    console.log('generating a new facebook access token using the old facebook access token');
    //var newToken = await oauth2.accessToken.create({ refresh_token: req.user.azureRefreshToken }).refresh();

    const options = {
        method: 'GET',
        uri: 'https://graph.facebook.com/v4.0/oauth/access_token?grant_type=fb_exchange_token',
        qs: {
            client_id: process.env.FB_CLIENT_ID,
            client_secret: process.env.FB_CLIENT_SECRET,
            fb_exchange_token: tmpToken
        }
    };
    request(options, (err, resp, body) => {
        if (err) return cb(err, null);

        if (resp.statusCode !== 200) {
            //res.status = resp.statusCode;
            //res.send('fbtokenrefresh failed:<br/>' + JSON.stringify(resp));
            let err = new Error(JSON.stringify(resp));
            return cb(err, null);
        }

        var rData = JSON.parse(body);

        console.log('new access token generated from old access token');
        console.log('new token:\n' + rData.access_token);

        if (req) {
            //update the user record with the new token information
            req.user.fbAccessToken = rData.access_token;
            req.user.fbAccessTokenExpires = parseFloat(new Date().getTime()) + (3600000 * 24); //1 day

            //req.login is exposed by passport - it updates the session and passport objects with the specified user. It calls passport.serializeUser()
            //================================
            req.login(req.user, (err) => {
                if (err) return cb(err, null);
                cb(null, rData.access_token);
            });
        } else {
            cb(null, rData.access_token);
        }

    });
}

module.exports.getFacebookToken = getFacebookToken;

function getGoogleAccessToken(req, tmpToken, tmpExp, tmpRefToken, cb) {

    if (!tmpToken && req) tmpToken = req.user.googleAccessToken;
    if (!tmpExp && req) tmpExp = req.user.googleAccessTokenExpires;
    if (!tmpRefToken && req) tmpRefToken = req.user.googleRefreshToken;

    //if we dont have the required info return a null
    if (!tmpToken || !tmpExp || !tmpRefToken) return cb(new Error('Some or all token info is missing'), null);
    //if the token has not expired then return the current token
    if (new Date().getTime() < tmpExp) return cb(null, tmpToken);

    //if the token has expired then try tpo get a new one with the refresh token.
    console.log('generating a new google access token using the old google refresh token');

    const options = {
        method: 'POST',
        uri: 'https://oauth2.googleapis.com/token',
        qs: {
            client_id: process.env.GOOGLE_CLIENT_ID,
            client_secret: process.env.GOOGLE_CLIENT_SECRET,
            refresh_token: tmpRefToken,
            grant_type: 'refresh_token'
        }
    };
    request(options, (err, resp, body) => {
        if (err) return cb(err, null);

        if (resp.statusCode !== 200) {
            //res.status = resp.statusCode;
            //res.send('fbtokenrefresh failed:<br/>' + JSON.stringify(resp));
            let err = new Error(JSON.stringify(resp));
            return cb(err, null);
        }

        var rData = JSON.parse(body);

        console.log('new google access token generated from refresh token');
        //console.log('new token:\n' + rData.access_token);
        console.log(JSON.stringify(rData));

        if (req) {
            //update the user record with the new token information
            req.user.googleAccessToken = rData.access_token;
            //req.user.googleAccessTokenExpires = parseFloat(new Date().getTime()) + 30000;
            req.user.googleAccessTokenExpires = parseFloat(new Date().getTime()) + ((rData.expires_in * 1000) - 300000); //subtract 5 min to allow for clock difs

            //req.login is exposed by passport - it updates the session and passport objects with the specified user. It calls passport.serializeUser()
            //================================
            req.login(req.user, (err) => {
                if (err) return cb(err, null);
                cb(null, rData.access_token);
            });
        } else {
            cb(null, rData.access_token);
        }

    });

}
module.exports.getGoogleAccessToken = getGoogleAccessToken;

function getStackpoleUser(method, authtoken, user, callBack) {
    if (!process.env.STACKPOLE_XML_SERVER) {
        let err = new Error('STACKPOLE_XML_SERVER is not defined');
        return callBack(err, null);
    }

    const options = {
        url: process.env.STACKPOLE_XML_SERVER + '/api/auth/userlogin.aspx',
        headers: {
            "content-type": "application/json",
            'x-sei-secret': process.env.STACKPOLE_LOGIN_SECRET
        },
        method: 'POST',
        json: { "method": method, "authkey": authtoken }
    };

    console.log("options:" + JSON.stringify(options));

    request(options, (err, resp, body) => {
        if (err) return callBack(err, null);

        if (resp.statusCode !== 200) {
            //let err = new Error(JSON.stringify(resp));
            let err = new Error("Response status responded in an error:" + resp.statusCode + " : " + resp.statusMessage);
            return callBack(err, null);
        }
        if (!body.ok) {
            let err = new Error("response body was not found");
            return callBack(err, null);
        } else if (!body.user) {
            let err = new Error("response had no user record");
            return callBack(err, null);
        } else {
            // push the needed data into the existing user record from the previous authorization
            //var stackuser = body.user;
            console.log("from sei user lookup call", body);
            user.xmlReqToken = body.user.xmlReqToken;
            user.sysid = body.user.sysid;
            user.companytype = body.user.companytype;
            user.userlocid = body.user.locid;
            user.seiusername = body.user.username;
            user.sysProfile = body.sysprofile;
            callBack(null, user);
        }
    });
}

function stackpoleUserCheck(username, password, cb) {

    if (!process.env.STACKPOLE_XML_SERVER) {
        let err = new Error('STACKPOLE_XML_SERVER is not defined');
        return cb(err, null);
    }

    const options = {
        url: process.env.STACKPOLE_XML_SERVER + '/api/auth/userlogin.aspx',
        headers: {
            "content-type": "application/json",
            'x-sei-secret': process.env.STACKPOLE_LOGIN_SECRET
        },
        method: 'POST',
        json: { "method": "local", "username": username, "password": password }
    };

    request(options, (err, resp, body) => {
        if (err) return cb(err, null);

        if (resp.statusCode !== 200) {
            let err = new Error(JSON.stringify(resp));
            return cb(err, null);
        }

        //var res = JSON.parse(body);

        console.log('got user from stackpole');
        //console.log('new token:\n' + rData.access_token);
        //console.log(JSON.stringify(body));

        if (!body.ok) {
            let err = new Error(body.err);
            return cb(err, null);
        } else if (!body.user) {
            let err = new Error(body.err);
            return cb(err, null);
        } else {
            var user = body.user;
            user.sysProfile = body.sysprofile;
            cb(null, user);
        }

    });

}



    

