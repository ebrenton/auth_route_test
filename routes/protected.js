var express = require('express');
var router = express.Router();
var auth = require('./auth');

/*  GET a protected page 
    Use the auth.verifyLoggedIn middleware for the test
 */
router.get('/', auth.verifyLoggedIn, function (req, res, next) {    
    //auth.verifyLoggedIn();
    res.send('If you see this then you must be logged in!<br /><br /><a href="/">Home</a>');
});

module.exports = router;
