const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const uuid = require('uuid/v4');
const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);

require('dotenv').config();

var indexRouter = require('./routes/index');
var graphRouter = require('./routes/graph');
var authRouter = require('./routes/auth');
var protectedRouter = require('./routes/protected');

const connect = mongoose.connect(process.env.MONGO_CN_URL, { useNewUrlParser: true });
connect.then((db) => {
    console.log('Connected to db');
}, (err) => { console.log(err); });

var app = express();

//Session Notes
//==============
// https://www.npmjs.com/package/express-session
// resave - (default = true) Forces the session to be saved back to the session store, even if the session was never modified during the request. Default = true. 
// saveUninitialized: false - Don't save the session until it has been initialized. Passport will initialize a session during login OR you can set a session variable (session.xxxx) and save it manually
// rolling: true, - Renew the cookie expiration date/time (based on maxAge) every time the session is touched.
// cookie.secure: false - ***!!! If true, only allow cookie over https ***IMPORTANT*** on Azure the actual server is not running in https, so this must be false
// httpOnly:true (default=true) Cookie is NOT visible in client javascript, it is only used to send to server. (can still be seen in browser settings, just not in client javascript code)

const cookieTime = (3600 * 1000 * 24) * process.env.SESSION_LIFETIME_DAYS;

const MongoStoreOptions = {
    mongooseConnection: mongoose.connection,
    ttl: cookieTime / 1000, // = 14 days. Default
    touchAfter: 24 * 3600, // time period in seconds (changes to the session will always update it, but otherwise, only update it this often)
    collection: (process.env.SESSION_COLLECTION || 'sessions')
};

app.use(session({
    genid: (req) => {
        //console.log('Inside the session middleware, generating a new session id');
        //console.log(req.sessionID);
        return uuid(); // use UUIDs for session IDs
    },
    store: new MongoStore(MongoStoreOptions),
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    rolling: true,
    cookie: {
        maxAge: cookieTime,
        secure: false
    }
}));


//view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//Server static pages in the 'public' folder, without any authentication needed.
//This is where you should store javascript includes, images, etc.
//Place this before passport stuff so deserialize user does not get called for these files.
app.use(express.static(path.join(__dirname, 'public')));

const passport = require('passport');
app.use(passport.initialize());
app.use(passport.session());

//if a request comes in that is NOT secure/https then redirect the request to the secure server (https) 
app.use(function (req, res, next) {
    if (!req.secure && process.env.REQUIRE_HTTPS === 'true') {
        //307 status code = target resource resides temporarily under a different url and user agent must not change the request method
        console.log('redirected to https');
        res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url);
        return;
    } else {
        return next();
    }
});


app.use('/', indexRouter);
app.use('/graph', graphRouter);
app.use('/auth', authRouter);
app.use('/protected', protectedRouter);

//Serve static pages in the 'private' folder, but only after verifying that the user is logged in.
//...this will actually prevent ANYTHING else from processing below this point, so this should be AFTER all 
//...your routes that do their own auth checking.
app.use(function (req, res, next) {
    if (req.isAuthenticated()) return next(); 
    console.log('Not logged in so redirecting to home page');
    res.redirect('/');
}, express.static(path.join(__dirname, 'private')));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development

    var eMsg = err.message;
    console.log('in error trap.\nError:', eMsg);

    //deserialize error will be generated if the session has a user but it does not validate for some reason.
    //this is self configured in passport.deserializeUser() (in auth route).
    //if this happens we want to fail gracefully so we'll kill the session and send them to some page...
    if (eMsg && eMsg.toString().slice(0, 17) === 'deserialize error') {
        
        req.session.destroy();
        res.clearCookie('connect.sid');
        console.log('session destroyed and cookie deleted');
        res.redirect('/sessionerror.html');

    } else {

        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    }
});

module.exports = app;
